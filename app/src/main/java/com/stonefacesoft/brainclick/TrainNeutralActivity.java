package com.stonefacesoft.brainclick;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.emotiv.insight.FacialExpressionDetection;
import com.emotiv.insight.IEmoStateDLL;

import java.util.Timer;
import java.util.TimerTask;

public class TrainNeutralActivity extends AppCompatActivity implements EngineInterface, View.OnClickListener, SeekBar.OnSeekBarChangeListener, AdapterView.OnItemSelectedListener {

    private static final Intent sSettingsIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);

    //Variables del Emotiv
    int userId = 0,count = 0,actionSelected = 555;
    EngineConnector engineConnector;
    public static float _currentPower = 0;
    boolean isTrainning = false;
    Timer timer;
    TimerTask timerTask;
    String currentRunningAction="";

    //Notif
    Notifications notifications;

    //Views
    Button TrainNeutral;
    FloatingActionButton BtnOK;
    Spinner SpinnerGesto;

    TextView Actual;
    ImageView ImgFace;
    SeekBar SeekGesto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_neutral);


        //Iniciando la conexion
        EngineConnector.setContext(this);
        engineConnector = EngineConnector.shareInstance();
        engineConnector.delegate = this;


        //Bindings
        TrainNeutral = (Button)findViewById(R.id.StartTrainNeutral);
        TrainNeutral.setOnClickListener(this);
        BtnOK = (FloatingActionButton) findViewById(R.id.BtnOk);
        BtnOK.setOnClickListener(this);

        Actual = (TextView)findViewById(R.id.ActualCommand);
        ImgFace = (ImageView) findViewById(R.id.ImgFace);

        SpinnerGesto = (Spinner) findViewById(R.id.SpinnerGesto);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.gestos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerGesto.setAdapter(adapter);
        SpinnerGesto.setOnItemSelectedListener(this);

        SeekGesto = (SeekBar) findViewById(R.id.SeekbarGesto);
        SeekGesto.setMax(1000);
        SeekGesto.setOnSeekBarChangeListener(this);

        notifications = new Notifications();
        notifications.SetContext(getApplicationContext());

        if (engineConnector.isConnected) {
            Log.e("TrainNeutral", "Conectado userId: " + userId);
            //TODO cambiar el gesto correspondiente
            SeekGesto.setProgress(FacialExpressionDetection.IEE_FacialExpressionGetThreshold(userId,
                    IEmoStateDLL.IEE_FacialExpressionAlgo_t.FE_SMILE.ToInt(),
                    FacialExpressionDetection.IEE_FacialExpressionThreshold_t.FE_SENSITIVITY.toInt())
                    [1]);

        }
        else
            Log.e("TrainNeutral","No Conectado");


        Timer timerListenAction = new Timer();
        timerListenAction.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHandlerUpdateUI.sendEmptyMessage(1);
            }
        },0,20);
    }

    @Override
    public void trainStarted() {
        Log.e("TrainNeutral","TrainStarted");
        timer = new Timer();
        intTimerTask();
        timer.schedule(timerTask ,0, 100);
    }

    @Override
    public void trainSucceed() {
        Log.e("TrainNeutral","TrainSucceed");
        new AlertDialog.Builder(this)
                .setTitle("Training Succeeded")
                .setMessage("Training is successful. Accept this training?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        engineConnector.setTrainControl(FacialExpressionDetection.IEE_FacialExpressionTrainingControl_t.FE_ACCEPT.getType());
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        engineConnector.setTrainControl(FacialExpressionDetection.IEE_FacialExpressionTrainingControl_t.FE_REJECT.getType());
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void trainCompleted() {
        Log.e("TrainNeutral","TrainCompleted");

    }

    @Override
    public void trainRejected() {
        Log.e("TrainNeutral","Train Rejected");
    }

    @Override
    public void trainErased() {
        Log.e("TrainNeutral","Train Erased");
    }

    @Override
    public void trainReset() {
        Log.e("TrainNeutral","Train Reset");
    }

    @Override
    public void userAdded(int userId) {
        Log.e("TrainNeutral","User added");
        this.userId = userId;
    }

    @Override
    public void userRemove() {
        Log.e("TrainNeutral","User Removed");
        this.userId = -1;
    }

    @Override
    public void detectedActionLowerFace(int typeAction, float power) {
        if (actionSelected == 2 || actionSelected == 3 ) {
            if (typeAction == IEmoStateDLL.IEE_FacialExpressionAlgo_t.FE_SMILE.ToInt()) {
//            Log.e("TrainActivity:", "Smile "+String.valueOf(power));
                if (power > 0.8) {
                    Actual.setText("Sonrisa grande");
                    ImgFace.setImageDrawable(getResources().getDrawable(R.drawable.big_smile));
                } else if (power <= 0.8 && power > 0.4) {
                    Actual.setText("Sonrisa");
                    ImgFace.setImageDrawable(getResources().getDrawable(R.drawable.smile));
                } else {
                    Actual.setText("Neutral sonrisa");
                    ImgFace.setImageDrawable(getResources().getDrawable(R.drawable.neutral));
                }
            }
            if (typeAction == IEmoStateDLL.IEE_FacialExpressionAlgo_t.FE_CLENCH.ToInt()) {
//            Log.e("TrainActivity:", "Smile "+String.valueOf(power));
                if (power > 0.7) {
                    Actual.setText("Mordida");
                    ImgFace.setImageDrawable(getResources().getDrawable(R.drawable.big_smile));
                } else {
                    Actual.setText("Neutral mordida");
                    ImgFace.setImageDrawable(getResources().getDrawable(R.drawable.neutral));
                }
            }
        }

    }

    @Override
    public void detectedActionUpperFace(int typeAction, float power) {
        if (actionSelected == 0 || actionSelected == 1 || actionSelected == 4 ) {

            if (typeAction == IEmoStateDLL.IEE_FacialExpressionAlgo_t.FE_SURPRISE.ToInt()) {
                if (power >= 0.7) {
                    Actual.setText("Sorpresa");
                    ImgFace.setImageDrawable(getResources().getDrawable(R.drawable.surprise));
                } else {
                    Actual.setText("Neutral sorpresa");
                    ImgFace.setImageDrawable(getResources().getDrawable(R.drawable.neutral));
                }
            }
            if (typeAction == IEmoStateDLL.IEE_FacialExpressionAlgo_t.FE_FROWN.ToInt()) {
                if (power >= 0.7) {
                    Actual.setText("Ceño fruncido");
                    ImgFace.setImageDrawable(getResources().getDrawable(R.drawable.surprise));
                } else {
                    Actual.setText("Neutral ceño");
                    ImgFace.setImageDrawable(getResources().getDrawable(R.drawable.neutral));
                }
            }
        }
    }

    @Override
    public void headsetStatus(int[] BatteryLevel, int[] ContactQ) {
//        Log.e("TrainActivity","Contact Q :"+ContactQ.length);
//        Log.e("TrainActivity","Battery level :"+BatteryLevel[0]);
        try {
            notifications.NotificarLowQ(ContactQ);
            notifications.NotificarBateriaBaja(BatteryLevel);
        }catch (NullPointerException e) {
            Log.e("TrainActivity", e.toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.StartTrainNeutral:
                Actual.setText("Entrenando Neutral");
                startTrainingFacialExpression(IEmoStateDLL.IEE_FacialExpressionAlgo_t.FE_NEUTRAL);
                break;
            case R.id.BtnOk:
                //TODO antes de lanzar el servicio tengo q guardar el umbral y q gesto esta entrenado y setearlo cuando lance el servicio
                if (!isMyServiceRunning(ClickerService.class)){
                    startActivity(sSettingsIntent);
                }

        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void intTimerTask(){
        count=0;
        timerTask = new TimerTask() {
            @Override
            public void run() {
                mHandlerUpdateUI.sendEmptyMessage(0);
            }
        };
    }
    public void startTrainingFacialExpression(IEmoStateDLL.IEE_FacialExpressionAlgo_t FacialExpressionAction) {
        isTrainning = engineConnector.startFacialExpression(isTrainning, FacialExpressionAction);
    }

    public Handler mHandlerUpdateUI = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    count ++;
                    int trainningTime=(int) FacialExpressionDetection.IEE_FacialExpressionGetTrainingTime(userId)[1]/1000;
                    if(trainningTime > 0)
                        Actual.setText("Entrenando "+String.valueOf(count/trainningTime)+"s");
                    if (count/trainningTime >= 16) {
                        Log.e("TrainActivity","Entrenando: "+String.valueOf(count/trainningTime));
                        timerTask.cancel();
                        timer.cancel();
                    }
                    break;
                case 1:
                    //moveimage
                    break;

                default:
                    break;
            }
        }
    };

    public void onBackPressed() {
        android.os.Process.killProcess(android.os.Process.myPid());
        finish();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()){
            case R.id.SeekbarGesto:
                switch (actionSelected){
                    //Pestanar
                    case 0:
                        FacialExpressionDetection.IEE_FacialExpressionSetThreshold(userId, IEmoStateDLL
                                .IEE_FacialExpressionAlgo_t.FE_BLINK.ToInt(), FacialExpressionDetection
                                .IEE_FacialExpressionThreshold_t.FE_SENSITIVITY.toInt(), progress);
                        break;
                    //sorpresa
                    case 1:
                        FacialExpressionDetection.IEE_FacialExpressionSetThreshold(userId, IEmoStateDLL
                                .IEE_FacialExpressionAlgo_t.FE_SURPRISE.ToInt(), FacialExpressionDetection
                                .IEE_FacialExpressionThreshold_t.FE_SENSITIVITY.toInt(), progress);
                        break;
                    //Sonrisa
                    case 2:
                        FacialExpressionDetection.IEE_FacialExpressionSetThreshold(userId, IEmoStateDLL
                                .IEE_FacialExpressionAlgo_t.FE_SMILE.ToInt(), FacialExpressionDetection
                                .IEE_FacialExpressionThreshold_t.FE_SENSITIVITY.toInt(), progress);
                        break;
                    //Mordida
                    case 3:
                        FacialExpressionDetection.IEE_FacialExpressionSetThreshold(userId, IEmoStateDLL
                                .IEE_FacialExpressionAlgo_t.FE_CLENCH.ToInt(), FacialExpressionDetection
                                .IEE_FacialExpressionThreshold_t.FE_SENSITIVITY.toInt(), progress);
                        break;
                    //Cenio
                    case 4:
                        FacialExpressionDetection.IEE_FacialExpressionSetThreshold(userId, IEmoStateDLL
                                .IEE_FacialExpressionAlgo_t.FE_FROWN.ToInt(), FacialExpressionDetection
                                .IEE_FacialExpressionThreshold_t.FE_SENSITIVITY.toInt(), progress);
                        break;
                }
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        actionSelected = position;

        switch (position){
            //Pestanar
            case 0:

                break;
            //sorpresa
            case 1:

                break;
            //Sonrisa
            case 2:

                break;
            //Mordida
            case 3:

                break;
            //Cenio
            case 4:

                break;


        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

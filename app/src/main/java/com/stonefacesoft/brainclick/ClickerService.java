package com.stonefacesoft.brainclick;

import android.accessibilityservice.AccessibilityService;
import android.app.Activity;
import android.app.Instrumentation;
import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Toast;

/**
 * Created by Héctor on 13/1/2017.
 */

//https://github.com/google/talkback/tree/12bdf2063e121a021f050c94cf5ebb2489c8af8a/src/main/java/com/android/switchaccess

public class ClickerService extends AccessibilityService {

    Notifications notificador;

    public ClickerService() {
        super();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        int eventType = event.getEventType();

        switch (eventType) {
            //Representa el evento de abrir un PopupWindow, Menu, Dialog, etc.
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                //TODO ver si es necesario
                break;

        }
    }

    @Override
    public void onInterrupt() {

    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        notificador = new Notifications();
        notificador.SetContext(getBaseContext());
        Toast.makeText(getApplicationContext(), R.string.StrOnServiceConn,Toast.LENGTH_SHORT).show();
    }



}

package com.stonefacesoft.brainclick;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.emotiv.insight.IEmoStateDLL;
import com.sababado.circularview.CircularView;
import com.sababado.circularview.Marker;

import java.util.Timer;


/* Esta activity se encarga de entrenar un determinado gesto facial
* primero toma una lectura del estado neutral y luego entra un gesto determinado*/

public class SetHBActivity extends AppCompatActivity implements EngineInterface{

    //Binding UX
    TextView AF3;
    TextView AF4;
    TextView T7;
    TextView T8;
    TextView Pz;
    float ContactQValue;

    CircularView circularView;
    MySimpleCircularViewAdapter mCircularAdapter;

    Button BtnNext;

    //Variables del Emotiv
    int userId = 0,count = 0;
    EngineConnector engineConnector;
    public static float _currentPower = 0;
    boolean isTrainning = false;
    String currentRunningAction="";

//push
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_hb);

        //Iniciando la conexion
        EngineConnector.setContext(this);
        engineConnector = EngineConnector.shareInstance();
        engineConnector.delegate = this;

        if (engineConnector.isConnected)
            Log.e("SetHBActivity","Conectado userId: "+userId);
        else
            Log.e("SetHBActivity","No Conectado");

        //Binding TODO mejorar esto
//        AF3 = (TextView)findViewById(R.id.AF3);
//        AF3.setBackgroundColor(getResources().getColor(android.R.color.black));
//        AF4 = (TextView)findViewById(R.id.AF4);
//        AF4.setBackgroundColor(getResources().getColor(android.R.color.black));
//        T7 = (TextView)findViewById(R.id.T7);
//        T7.setBackgroundColor(getResources().getColor(android.R.color.black));
//        T8 = (TextView)findViewById(R.id.T8);
//        T8.setBackgroundColor(getResources().getColor(android.R.color.black));
//        Pz = (TextView)findViewById(R.id.Pz);
//        Pz.setBackgroundColor(getResources().getColor(android.R.color.black));

        //Circular View
        mCircularAdapter = new MySimpleCircularViewAdapter();
        circularView = (CircularView) findViewById(R.id.circular_view);
        circularView.setAdapter(mCircularAdapter);

        BtnNext = (Button)findViewById(R.id.BtnTrainNeutral);
        BtnNext.setEnabled(false);
        BtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.BtnTrainNeutral:
                        Intent intent = new Intent(SetHBActivity.this, TrainNeutralActivity.class);
                        startActivity(intent);
                        break;
                }

            }
        });
    }

    @Override
    public void trainStarted() {
        Log.e("SetHBActivity","TrainStarted");
    }

    @Override
    public void trainSucceed() {
        Log.e("SetHBActivity","TrainSucced");
    }

    @Override
    public void trainCompleted() {
        Log.e("SetHBActivity","TrainCompleted");
    }

    @Override
    public void trainRejected() {
        Log.e("SetHBActivity","Train Rejected");
    }

    @Override
    public void trainErased() {
        Log.e("SetHBActivity","Train Erased");
    }

    @Override
    public void trainReset() {
        Log.e("SetHBActivity","Train Erased");
    }

    @Override
    public void userAdded(int userId) {
        Log.e("SetHBActivity","User added");
        this.userId = userId;
    }

    @Override
    public void userRemove() {
        Log.e("SetHBActivity","Train Removed");
        this.userId = -1;
    }

    //5 input channels: AF3, AF4, T7, T8, Pz
    @Override
    public void headsetStatus(int[] BatteryLevel,int[] ContactQ) {
        mCircularAdapter.SetContactQData(ContactQ);
        mCircularAdapter.notifyDataSetChanged();
        ContactQValue = mCircularAdapter.GetContactValue();
        circularView.setText("Contact Quality "+String.valueOf(ContactQValue));
        Log.e("SetHBActivity","Contact Q :"+ContactQValue);
        if (ContactQValue >= 50)
            BtnNext.setEnabled(true);
        else
            BtnNext.setEnabled(false);
        Log.e("SetHBActivity","Battery level :"+BatteryLevel[0]);
    }

    @Override
    public void detectedActionLowerFace(int typeAction, float power) {
        _currentPower=power;
        Log.e("SetHBActvity:", "LowerFace");
    }

    @Override
    public void detectedActionUpperFace(int typeAction, float power) {
        Log.e("SetHBActvity:", "UpperFace");
    }

    public void onBackPressed() {
        android.os.Process.killProcess(android.os.Process.myPid());
        finish();
    }
}


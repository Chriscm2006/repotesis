package com.stonefacesoft.brainclick;

import android.graphics.drawable.Drawable;
import android.widget.TextView;

import com.sababado.circularview.Marker;
import com.sababado.circularview.SimpleCircularViewAdapter;

/**
 * Created by Héctor on 10/2/2017.
 */

public class MySimpleCircularViewAdapter extends SimpleCircularViewAdapter {
    private int[] ContactQData;
    float ContactQValue;

    @Override
    public int getCount() {
        // This count will tell the circular view how many markers to use.
        return 5;
    }

    @Override
    public void setupMarker(final int position, final Marker marker) {
        // Setup and customize markers here. This is called every time a marker is to be displayed.
        // 0 >= position > getCount()
        // The marker is intended to be reused. It will never be null.

        if (ContactQData == null) {
            return;
        }

        switch (position){
            //Pz
            case 0:
                marker.setSrc(SetContactDrawable(ContactQData[9]));
                marker.setFitToCircle(true);
                marker.setRadius(50);
                break;
            //T7
            case 1:
                marker.setSrc(SetContactDrawable(ContactQData[7]));
                marker.setFitToCircle(true);
                marker.setRadius(50);
                break;
            //AF3
            case 2:
                marker.setSrc(SetContactDrawable(ContactQData[3]));
                marker.setFitToCircle(true);
                marker.setRadius(50);
                break;
            //AF4
            case 3:
                marker.setSrc(SetContactDrawable(ContactQData[16]));
                marker.setFitToCircle(true);
                marker.setRadius(50);
                break;
            //T8
            case 4:
                marker.setSrc(SetContactDrawable(ContactQData[12]));
                marker.setFitToCircle(true);
                marker.setRadius(50);
                break;
            default:
                marker.setSrc(R.drawable.surprise);
                marker.setFitToCircle(true);
                marker.setRadius(50);
                break;
        }
    }


    public void SetContactQData(int[] Data){
        ContactQData = Data;
        ContactQValue = (float)((Data[3]+Data[16]+Data[7]+Data[12]+Data[9])/5)/4*100;
    }

    public float GetContactValue(){
        return ContactQValue;
    }
    private int SetContactDrawable (int Q){
        switch (Q){
            case 0:
                return R.mipmap.no_signal;
            case 1:
                return R.mipmap.bad_signal;
            case 2:
                return R.mipmap.mid_signal;
            case 3:
                return R.mipmap.mid_good_signal;
            case 4:
                return R.mipmap.good_signal;
            default:
                return R.mipmap.ic_launcher;
        }
    }
}
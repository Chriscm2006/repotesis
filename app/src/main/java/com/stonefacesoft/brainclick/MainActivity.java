package com.stonefacesoft.brainclick;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.app.ActionBar.LayoutParams;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.emotiv.insight.FacialExpressionDetection;

import java.util.Timer;
import java.util.TimerTask;

/* Es la activity principal, desde aca se habilita el servicio, se entrena la headband y se
prueba para que tutto funcione correctamente.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final Intent sSettingsIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);

    Button BtnListo;
    ImageSwitcher Switcher;
    boolean Imagen1 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BtnListo = (Button)findViewById(R.id.button);
        BtnListo.setOnClickListener(this);

        Switcher = (ImageSwitcher) findViewById(R.id.ImageSwitcher);
        Switcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView myView = new ImageView(getApplicationContext());
                myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                myView.setLayoutParams(new
                        ImageSwitcher.LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT));
                return myView;
            }
        });
        Animation in = AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right);
        Switcher.setInAnimation(in);
        Switcher.setOutAnimation(out);

        Switcher.setImageResource(R.drawable.frente);
        Imagen1 = true;

        Timer timerChangeImage = new Timer();
        timerChangeImage.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHandlerUpdate.sendEmptyMessage(0);
            }
        },0,3000);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                Intent intent = new Intent(this, SetHBActivity.class);
                startActivity(intent);
        }
    }

    public Handler mHandlerUpdate = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    //TODO editar las imagenes como la de la cabeza de arriba
                    if (Imagen1){
                        Switcher.setImageResource(R.drawable.perfil1);
                        Imagen1 = false;
                    }else {
                        Switcher.setImageResource(R.drawable.frente1);
                        Imagen1 = true;
                    }
                    break;

                default:
                    break;
            }
        }
    };
}

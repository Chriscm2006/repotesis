package com.stonefacesoft.brainclick;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

/*- Esta clase muestra el logo de la app al iniciar, y se asegura de tener conexion al Bluetooth.
Maneja ademas los permisos para Android +6.0, una vez que se tienen los permiss necesarios se
continua adelante.-*/

public class SplashScreen extends Activity {

    // Definimos la duración del splash screen
    private static final long SPLASH_SCREEN_DELAY = 2000;

    //Variables para la conexion Bluetooth
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int MY_PERMISSIONS_REQUEST_BLUETOOTH = 0;
    private BluetoothAdapter mBluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Escondemos la barra de Titulo
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash_screen);
        findViewById(R.id.ImgLogoApp).startAnimation(
                AnimationUtils.loadAnimation(this, R.anim.heartbeat));


        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        //Checkeo si la version de Android es mayor a Marshmallow, si es mayor checkeo si tengo
        //permiso, sino lo pido. Si la version es menor, diectamente checkeo si tengo conexion
        //con el Bluetooth
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /***Android 6.0 and higher need to request permission*****/
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH)
                    != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.BLUETOOTH},
                                MY_PERMISSIONS_REQUEST_BLUETOOTH);
            }
            else{
                checkConnect();
            }
        }
        else {
            checkConnect();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_BLUETOOTH: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkConnect();
                } else {
                    // permission negada, explicar al usuario que esta app necesita bluetooth.
                    Toast.makeText(this, R.string.BluetoothPermFail, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK){
                NextActivity();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, R.string.BluetoothConnFail
                        , Toast.LENGTH_SHORT).show();
            }
        }
    }



    //Checkeo si puedo conectar con el Bluetooth
    private void checkConnect(){
        if (!mBluetoothAdapter.isEnabled()) {
            //Prender Bluetooth
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        else {
            //Ir a MainActivity y seguir con la app
            NextActivity();
        }
    }

    private void NextActivity(){
        //Task para la demora y el paso a MainActivity
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // Llamamos a la Actividad principal de la aplicacion
                Intent mainIntent = new Intent().setClass(
                        SplashScreen.this, MainActivity.class);
                startActivity(mainIntent);
                // Cerramos la actividad para que el usuario no pueda volver a ella presionando
                // el boton atrás
                // TODO probar q funcione
                SplashScreen.this.finish();
            }
        };
        // Simulación de un proceso de carga al iniciar la aplicación.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }
}